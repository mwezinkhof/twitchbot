## Getting started
* Install java jdk 1.7.*
* Install maven 3.*
* add the maven bin to your PATH
* run mvn compile
* run the bot once, so the config file is generated
* install MySQL
* run the twitch.sql
* configure the connection
* hf

## BUILD
You can just run the Launcher.java class from your IDE

OR

If you want to make a standalone jar use the following command

```
mvn clean compile assembly:single
```

