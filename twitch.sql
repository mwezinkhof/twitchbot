/*
MySQL Data Transfer
Source Host: localhost
Source Database: twitch
Target Host: localhost
Target Database: twitch
Date: 15-9-2014 12:36:01
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for chatlog
-- ----------------------------
DROP TABLE IF EXISTS `chatlog`;
CREATE TABLE `chatlog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `channel` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `user` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `chattime` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for command
-- ----------------------------
DROP TABLE IF EXISTS `command`;
CREATE TABLE `command` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `channel` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `input` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `output` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for rpg
-- ----------------------------
DROP TABLE IF EXISTS `rpg`;
CREATE TABLE `rpg` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `viewer` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `level` int(11) NOT NULL,
  `xp` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for text_template
-- ----------------------------
DROP TABLE IF EXISTS `text_template`;
CREATE TABLE `text_template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `channel` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `keyphrase` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `text` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for viewers
-- ----------------------------
DROP TABLE IF EXISTS `viewers`;
CREATE TABLE `viewers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `firsttime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lasthello` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records 
-- ----------------------------
INSERT INTO `command` VALUES ('1', 'kaazinator', 'creator', 'Hodor created me.');
INSERT INTO `command` VALUES ('3', 'kaazinator', 'kaaz\'s', 'lolking -> http://www.lolking.net/summoner/euw/20350817');
INSERT INTO `command` VALUES ('4', 'kaazinator', 'lolking', 'kaaz\'s lolking -> http://www.lolking.net/summoner/euw/20350817');
INSERT INTO `command` VALUES ('6', 'mushisgosu', '!ask', 'gosu\'s ask.fm is http://ask.fm/lolgosu1 You can ask him anything there!');
INSERT INTO `text_template` VALUES ('1', 'xlkaaz', 'bot_startup', 'You dare summon me?');
INSERT INTO `text_template` VALUES ('2', 'xlkaaz', 'bot_startup', 'Guess who\'s back!');
INSERT INTO `text_template` VALUES ('3', 'xlkaaz', 'bot_startup', 'I bet you missed me!');
INSERT INTO `text_template` VALUES ('4', 'xlkaaz', 'bot_startup', 'Back, and better than ... last time!');
INSERT INTO `text_template` VALUES ('5', 'xlkaaz', 'bot_is_op', 'I GOT THE POWER!');
INSERT INTO `text_template` VALUES ('6', 'xlkaaz', 'bot_is_op', 'I\'m da boss now');
INSERT INTO `text_template` VALUES ('7', 'xlkaaz', 'bot_is_op', 'Be nice to me and I might not kick you');
INSERT INTO `text_template` VALUES ('8', 'xlkaaz', 'bot_is_op', 'I\'m op now!');
INSERT INTO `text_template` VALUES ('9', 'xlkaaz', 'found_op', 'I\'ve missed you %s! My favorite OP BibleThump');
INSERT INTO `text_template` VALUES ('10', 'xlkaaz', 'found_op', 'Say hi to my favorite OP %s!');
INSERT INTO `text_template` VALUES ('11', 'xlkaaz', 'found_op', 'Why is %s even an OP?');
INSERT INTO `text_template` VALUES ('12', 'xlkaaz', 'found_op', 'A new mod appears! Hi %s!');
INSERT INTO `text_template` VALUES ('13', 'xlkaaz', 'found_op', 'Are you gonna help me %s?');
INSERT INTO `text_template` VALUES ('14', 'xlkaaz', 'found_op', 'Do you even OP, %s?');
INSERT INTO `text_template` VALUES ('15', 'xlkaaz', 'unknown_command', 'Did you expect me to do anything? Kappa  Check !help');
INSERT INTO `text_template` VALUES ('16', 'xlkaaz', 'unknown_command', 'Dafuq, who uses that command. Use !help');
INSERT INTO `text_template` VALUES ('17', 'xlkaaz', 'unknown_command', 'Unknown command, try !help');
INSERT INTO `text_template` VALUES ('18', 'xlkaaz', 'unknown_command', 'You might wanna use !help to see what commands exist');
INSERT INTO `text_template` VALUES ('19', 'xlkaaz', 'permission_denied', 'Nice try Kappa');
INSERT INTO `text_template` VALUES ('20', 'xlkaaz', 'permission_denied', 'NOPE!');
INSERT INTO `text_template` VALUES ('21', 'xlkaaz', 'permission_denied', 'You\'re not allowed to use that command');
INSERT INTO `text_template` VALUES ('22', 'xlkaaz', 'permission_denied', 'Bitch please');
INSERT INTO `text_template` VALUES ('23', 'xlkaaz', 'permission_denied', 'Only big boys can use that');
INSERT INTO `text_template` VALUES ('24', 'xlkaaz', 'bot_startup', 'Your wish is my command');
INSERT INTO `text_template` VALUES ('25', 'xlkaaz', 'welcome_new_viewer', 'Hey %s!');
INSERT INTO `text_template` VALUES ('26', 'xlkaaz', 'welcome_new_viewer', 'Yo %s!');
INSERT INTO `text_template` VALUES ('27', 'xlkaaz', 'welcome_new_viewer', 'Welcome %s!');
INSERT INTO `text_template` VALUES ('28', 'xlkaaz', 'welcome_new_viewer', 'Say hello to %s!');
INSERT INTO `text_template` VALUES ('29', 'xlkaaz', 'welcome_new_viewer', 'Hi %s!');
INSERT INTO `text_template` VALUES ('30', 'xlkaaz', 'welcome_new_viewer', 'Welcome to the stream %s!');
INSERT INTO `text_template` VALUES ('31', 'xlkaaz', 'welcome_back_viewer', 'Welcome back %s!');
INSERT INTO `text_template` VALUES ('32', 'xlkaaz', 'welcome_back_viewer', 'Glad to see you back %s!');
INSERT INTO `text_template` VALUES ('33', 'xlkaaz', 'welcome_back_viewer', 'Glad you enjoyed it enough to come back %s :D');
INSERT INTO `text_template` VALUES ('34', 'xlkaaz', 'welcome_back_viewer', 'Happy to see you back %s!');
INSERT INTO `text_template` VALUES ('35', 'xlkaaz', 'welcome_back_viewer', 'glad to see you return %s :D');
INSERT INTO `text_template` VALUES ('36', 'xlkaaz', 'welcome_back_viewer', 'I can\'t believe you returned %s! BibleThump');
INSERT INTO `text_template` VALUES ('37', 'xlkaaz', 'spam_warning', 'Please stop the spamerino %s.');
INSERT INTO `text_template` VALUES ('38', 'xlkaaz', 'spam_warning', 'Can you please stop %s?');
INSERT INTO `text_template` VALUES ('39', 'xlkaaz', 'spam_warning', 'Don\'t spam please %s!');
INSERT INTO `text_template` VALUES ('40', 'xlkaaz', 'unknown_command', 'Thats not even a command FailFish Check !help');
