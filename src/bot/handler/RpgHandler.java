package bot.handler;

import bot.Bot;
import db.mysql.Db;
import misc.StringUtil;
import org.apache.log4j.Logger;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class RpgHandler extends Thread {

	ScheduledExecutorService executor =
			Executors.newSingleThreadScheduledExecutor();
	private Bot bot;
	public static int xpPerLevel = 10; // xpPerlevel*currentlevel = needed xp for level up
	private static final Logger log = Logger.getLogger(RpgHandler.class);
	private ArrayList<String> lastViewerList, newViewerList,checkedPlayers;

	Runnable periodicTask = new Runnable() {
		public void run() {
			handleRewards();
			checkForLevelUp();
		}
	};
	private void handleRewards(){
		log.info("RPG system iteration");
		newViewerList = bot.getPlayerList();
		log.info("found viewers: " + newViewerList.toString());
		ArrayList<String> xpRecievers = new ArrayList<>();
		xpRecievers.add(bot.defaultChannel);
		for(String viewer : newViewerList){
			if(lastViewerList.contains(viewer)){
				log.info("+1 xp for : " + viewer);
				xpRecievers.add(viewer);
			}
			else if(!checkedPlayers.contains(viewer)){
				try(ResultSet rs = Db.select("SELECT viewer FROM rpg WHERE viewer = ? AND channel = ?", viewer, bot.defaultChannel)){
					if(!rs.next()){
						Db.query("INSERT INTO rpg(viewer, level, xp, channel) VALUES(?,1,1,?) ",viewer,bot.defaultChannel);
					}
					checkedPlayers.add(viewer);
				}
				catch (SQLException e){
					log.error(e);
				}
			}
		}
		if(xpRecievers.size()>1){
			Db.query("UPDATE rpg SET xp = xp+1 WHERE channel = ? AND viewer IN("+ StringUtil.repeatQuestionMark(xpRecievers.size()-1)+")",xpRecievers);
		}
		lastViewerList.clear();
		lastViewerList.addAll(newViewerList);

	}
	private void checkForLevelUp(){
		try(ResultSet r = Db.select("SELECT id,viewer, level FROM rpg WHERE xp >= level*"+xpPerLevel+" and channel = ?",bot.defaultChannel)){
			while(r.next()){
				Db.query("UPDATE rpg set xp = xp - ?, level = level+1 WHERE id = ?", xpPerLevel*r.getInt("level"),r.getInt("id"));
				bot.msg(String.format("%s is now level %s!",r.getString("viewer"),r.getInt("level")+1));
			}
		}
		catch(SQLException e){
			log.error(e);
		}
	}
	public RpgHandler() {
		lastViewerList = new ArrayList<>();
		newViewerList = new ArrayList<>();
		checkedPlayers = new ArrayList<>();
	}
	public void setBot(Bot b) {
		bot = b;
		start();
	}

	public void run() {
		executor.scheduleAtFixedRate(periodicTask, 0, 5, TimeUnit.MINUTES);
	}
}