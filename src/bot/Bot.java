package bot;

import bot.handler.CommandHandler;
import bot.handler.RpgHandler;
import bot.handler.TemplateHandler;
import bot.handler.TimeManager;
import bot.thread.ChatLogger;
import bot.thread.ViewerHistory;
import chatlog.LogMessage;
import main.Config;
import org.apache.log4j.Logger;
import org.jibble.pircbot.IrcException;
import org.jibble.pircbot.PircBot;
import org.jibble.pircbot.User;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.TimerTask;

/**
 * This is the main bot class
 */

public class Bot extends PircBot {
    public final String defaultChannel;
    public final String commandPrefix = "!";
    private final TimeManager timer;
    private final Logger log = Logger.getLogger(Bot.class);
    private final Config config = Config.getInstance();
    private final CommandHandler commandHandler;
    private final ChatLogger chatLog;
    private final ViewerHistory viewerHistory;
    private boolean amIOp = false;
    private HashMap<String, ArrayList<String>> opList;
    private ArrayList<String> seenViewers;
    private RpgHandler rpgHandler;

    public Bot(String nickname, String channel, String auth) {
        chatLog = new ChatLogger();
        viewerHistory = new ViewerHistory();
        defaultChannel = channel;
        opList = new HashMap<>();
        timer = new TimeManager();
        seenViewers = new ArrayList<>();
        viewerHistory.setBot(this);
        setVerbose(true);
        commandHandler = new CommandHandler(this);
        setName(nickname);
        connectToIrc(channel, auth);
        if (isConnected()) {
            commandHandler.load();
            msg(TemplateHandler.get("bot_startup"));
            rpgHandler = new RpgHandler();
            rpgHandler.setBot(this);
        }
    }

    public void addCustomCommand(String command, String output) {
        commandHandler.addCustomCommand(command, output);
    }

    public void removeCustomCommand(String command) {
        commandHandler.removeCustomCommand(command);
    }

    public String[] getCommands() {
        return commandHandler.getCommands();
    }

    public String[] getCustomCommands() {
        return commandHandler.getCustomCommands();
    }

    protected void connectToIrc(String channel, String password) {
        try {
            this.connect(config.get("irc.host"), Integer.parseInt(config.get("irc.port")), password);
            this.joinChannel("#" + channel);
            addOp("#" + defaultChannel, defaultChannel);
        } catch (IOException | IrcException e) {
            log.fatal("Couldn't connect to irc, make sure the settings are correct!");
        }
    }

    public void schedule(TimerTask t, int seconds) {
        timer.schedule(t, seconds * 1000);
    }

    public void repeat(TimerTask t, int delay, int repeatDelay) {
        timer.scheduleAtFixedRate(t, delay * 1000, repeatDelay * 1000);
    }

    @Override
    protected void onMessage(String channel, String sender, String login, String hostname, String message) {
        super.onMessage(channel, sender, login, hostname, message);
        if (message.startsWith(commandPrefix) && message.length() > 1) {
            commandHandler.process(sender, message, isOp(sender));
        } else {
            chatLog.log(new LogMessage(channel, sender, message));
        }
    }

    public ArrayList<String> getPlayerList() {
        ArrayList<String> viewerList = new ArrayList<>();
        User[] rawUserList = getUsers("#" + defaultChannel);
        for (User u : rawUserList) {
            if (!u.getNick().equals(getName())) {
                viewerList.add(u.getNick());
            }
        }
        return viewerList;
    }

    private void addOp(String channel, String user) {
        if (user.equals(this.getName())) {
            msg(TemplateHandler.get("bot_is_op"));
            amIOp = true;
        }
        if (!opList.containsKey(channel)) {
            opList.put(channel, new ArrayList<String>());
        }
        ArrayList<String> userList = opList.get(channel);
        if (!user.equals(this.getName())) {
            if (!userList.contains(user)) {
                userList.add(user);
                if (!user.equals(defaultChannel))
                    msg(String.format(TemplateHandler.get("found_op"), user));
            }
        }
    }

    private void removeOp(String channel, String user) {
        if (user.equals(this.getName())) {
            amIOp = false;
        }
        if (opList.containsKey(channel)) {
            if (opList.get(channel).contains(user)) {
                opList.get(channel).remove(user);
            }
        }
    }

    @Override
    protected void onUserMode(String targetNick, String sourceNick, String sourceLogin, String sourceHostname, String mode) {
        super.onUserMode(targetNick, sourceNick, sourceLogin, sourceHostname, mode);
        String[] userNode = mode.split(" ");
        if (userNode.length == 3) {
            if (userNode[1].equals("+o")) {
                addOp(userNode[0], userNode[2]);
            } else if (userNode[1].equals("-o")) {
                removeOp(userNode[0], userNode[2]);
            }
        }
    }

    public void msg(String text) {
        if (text.length() > 0) {
            //this.sendMessage("#" + this.defaultChannel, text);
        }
    }

    @Override
    protected void onPrivateMessage(String sender, String login, String hostname, String message) {
        onMessage(defaultChannel, sender, login, hostname, message);
    }

    public boolean isOp(String nickname) {
        return isOp("#" + defaultChannel, nickname);
    }

    public boolean isOp(String channel, String nickname) {
        if (opList.containsKey(channel)) {
            return opList.get(channel).contains(nickname);
        }
        return false;
    }

    @Override
    protected void onPart(String channel, String sender, String login, String hostname) {
        seenViewers.remove(sender);
    }

    @Override
    protected void onJoin(String channel, String sender, String login, String hostname) {
        viewerHistory.checkPlayer(sender);
        if (!sender.equals(this.getName())) {
            if (!seenViewers.contains(sender)) {
                seenViewers.add(sender);
            }
        }
    }
}
