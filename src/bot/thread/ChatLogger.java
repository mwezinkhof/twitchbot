package bot.thread;

import chatlog.LogMessage;
import db.mysql.Db;
import org.apache.log4j.Logger;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.LinkedBlockingQueue;

public class ChatLogger extends Thread {
	private LinkedBlockingQueue<LogMessage> itemsToLog =
			new LinkedBlockingQueue<>();
	private volatile boolean loggerTerminated;
	private ArrayList<String> ignoredUsers;
	private final Logger log = Logger.getLogger(ChatLogger.class);

	public ChatLogger() {
		start();
		ignoredUsers = new ArrayList<>();
		ignoredUsers.add("jtv");
	}

	public void run() {
		try {
			LogMessage logMessage;
			while (true) {
				logMessage = itemsToLog.take();
				if (!ignoredUsers.contains(logMessage.user)) {
					if (logMessage.channel.startsWith("#")) {
						logMessage.channel = logMessage.channel.substring(1);
					}
					Db.query("INSERT INTO chatlog(channel, user, message,chattime) VALUES(?, ?, ?,?)", logMessage.channel, logMessage.user, logMessage.message, new Timestamp(new Date().getTime()).toString());
					//log.info(String.format("%1$10s: %2$s",logMessage.user,logMessage.message));
				}
			}
		} catch (InterruptedException iex) {
		} finally {
			loggerTerminated = true;
		}
	}

	public void log(LogMessage lm) {
		if (loggerTerminated) return;
		try {
			itemsToLog.put(lm);
		} catch (InterruptedException iex) {
			Thread.currentThread().interrupt();
			throw new RuntimeException("Unexpected interruption");
		}
	}
}