package bot.thread;

import bot.Bot;
import bot.handler.TemplateHandler;
import db.mysql.Db;
import misc.StringUtil;
import org.apache.log4j.Logger;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Checks if a user has been seen before
 * Tells the message "welcome_new_player" if its the first time for a viewer
 * tells the message "welcome_back_viewer" if viewer returns after a day or longer [nothing before!]
 * Only sends out a message once per minute in order to reduce spam!
 */
public class ViewerHistory extends Thread {

	private LinkedBlockingQueue<String> playersToCheck =
			new LinkedBlockingQueue<>();
	private Bot bot;
	private Logger log = Logger.getLogger(ViewerHistory.class);
	private String currentTimeStamp;
	private volatile boolean threadTerminated;
	private ArrayList<String> newViewers;
	private ArrayList<String> returningViewers;
	private Calendar nextGreetTime;

	public ViewerHistory() {
		newViewers = new ArrayList<>();
		returningViewers = new ArrayList<>();
		nextGreetTime = Calendar.getInstance();
		nextGreetTime.setTime(new Date());
		nextGreetTime.add(Calendar.MINUTE, 1);
		start();
	}

	public void setBot(Bot b) {
		bot = b;
	}

	public void run() {
		try {
			String userName;
			while (true) {
				userName = playersToCheck.take();
				currentTimeStamp = new Timestamp(new Date().getTime()).toString();
				Calendar currentDate = Calendar.getInstance();

				try (ResultSet r = Db.select("select username,firsttime, lasthello  from viewers where username = ?", userName)) {
					if (!r.next()) {
						registerViewer(userName);
					} else {

						Calendar lastGreetDate = Calendar.getInstance();
						lastGreetDate.setTime(Timestamp.valueOf(r.getString("lasthello")));
						lastGreetDate.add(Calendar.DATE, 1);
						if (lastGreetDate.before(currentDate)) {
							updateLastGreetFor(userName);
						}
					}
				} catch (SQLException uselessError) {
				}
				if (currentDate.after(nextGreetTime)) {
					log.info("Greeting new Viewers!@" + currentTimeStamp);
					greetViewers();
					nextGreetTime.setTime(new Date());
					nextGreetTime.add(Calendar.MINUTE, 1);
				}
			}
		} catch (InterruptedException iex) {
		} finally {
			threadTerminated = true;
		}
	}

	public void greetViewers() {
		if (newViewers.size() > 0) {
			bot.msg(String.format(TemplateHandler.get("welcome_new_viewer"), StringUtil.enumerateList(newViewers)));
			newViewers.clear();
		}
		if (returningViewers.size() > 0) {
			bot.msg(String.format(TemplateHandler.get("welcome_back_viewer"), StringUtil.enumerateList(returningViewers)));
			returningViewers.clear();
		}
	}

	public void registerViewer(String userName) {
		Db.query("INSERT INTO viewers(username,firsttime,lasthello) VALUES(?,?,?)", userName, currentTimeStamp, currentTimeStamp);
		newViewers.add(userName);
	}

	public void updateLastGreetFor(String userName) {
		Db.query("UPDATE viewers SET lasthello = ? WHERE username = ?", currentTimeStamp, userName);
		returningViewers.add(userName);
	}

	public void checkPlayer(String lm) {
		if (threadTerminated) return;
		try {
			playersToCheck.put(lm);
		} catch (InterruptedException iex) {
			Thread.currentThread().interrupt();
			throw new RuntimeException("Unexpected interruption");
		}
	}
}
