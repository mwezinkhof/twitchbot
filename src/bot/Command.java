package bot;

import org.apache.log4j.Logger;

public abstract class Command {

	protected String name = "My unimplemented command";
	protected String description = "Unset description";
	protected String cmd = "unimplemented";
	protected Bot bot;
	protected Logger log;
	protected boolean requiresOp=true;

	protected void setOpOnly(boolean opOnly){
		requiresOp = opOnly;
	}
	public boolean isOpOnly(){
		return requiresOp;
	}
	protected void setDescription(String d){
		this.description = d;
	}
	public String getDescription(){
		return description;
	}
	public Command(Bot bot) {
		log = Logger.getLogger(this.getClass());
		this.bot = bot;
	}

	public Command() {

	}

	public String getCmd() {
		return cmd;
	}

	protected void setCmd(String c) {
		cmd = c;
	}

	public String getUsage() {
		return "Type !" + cmd;
	}

	public abstract String execute(String[] args, String sender, boolean isOp);
}
