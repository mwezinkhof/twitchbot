package bot.commands;

import bot.Bot;
import bot.Command;

public class Help extends Command {
	public Help(Bot b) {
		super(b);
		setCmd("help");
		setOpOnly(false);
	}

	@Override
	public String execute(String[] args, String sender, boolean isOp) {
		String com="The available commands are: ";
		boolean first = true;
		for(String s: bot.getCommands()){
			if(first){
				first=false;
				com += s;
			}
			else {
				com += ", " + s;
			}
		}
		for(String s: bot.getCustomCommands()){
			com += ", " + s;
		}
		return com;
	}
}
