package bot.commands;

import bot.Bot;
import bot.Command;
import bot.handler.RpgHandler;
import db.mysql.Db;

import java.sql.ResultSet;
import java.sql.SQLException;

public class RolePlayingGame extends Command {

	public RolePlayingGame(Bot bot) {
		super(bot);
		setCmd("rpg");
		setDescription("Handling rpg stuff!");
	}

	@Override
	public String execute(String[] args, String sender, boolean isOp) {
		try (ResultSet r = Db.select("SELECT viewer, level, xp FROM rpg where viewer = ? AND channel = ?", sender,bot.defaultChannel)) {
			if (r.next()) {
				float level = r.getInt("level");
				float xp = r.getInt("xp");
				float percent = (xp / (RpgHandler.xpPerLevel * level)) * 100;
				return String.format("@%s you are level %s and your xp bar is at %s%%!", r.getString("viewer"), (int) level, (long) percent);
			}
		} catch (SQLException e) {
			log.error(e);
		}
		return "You haven't been here long enough!";
	}
}
