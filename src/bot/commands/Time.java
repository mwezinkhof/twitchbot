package bot.commands;

import bot.Bot;
import bot.Command;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Time extends Command {
	DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
	public Time(Bot b) {
		super(b);
		setCmd("time");
		setOpOnly(false);
	}

	@Override
	public String execute(String[] args, String sender, boolean isOp) {
		return dateFormat.format(Calendar.getInstance().getTime());
	}
}
