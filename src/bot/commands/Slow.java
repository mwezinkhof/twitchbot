package bot.commands;

import bot.Bot;
import bot.Command;
import bot.handler.TemplateHandler;

public class Slow extends Command {

	public Slow(Bot b) {
		super(b);
		setCmd("mod");
		setOpOnly(true);
	}

	@Override
	public String execute(String[] args, String sender, boolean isOp) {
		if(isOp && args.length >= 1){
			if(args[0].equals("off")||args[0].equals("stop")){
				return ".slowoff";
			}
			if(isInteger(args[0])){
				return ".slow "+args[0];
			}
			return "I'm gonna need you to give me a (positive) number";
		}
		return TemplateHandler.get("permission_denied");
	}
	private boolean isInteger(String str) {
		if (str == null) {
			return false;
		}
		int length = str.length();
		if (length == 0) {
			return false;
		}
		if (str.charAt(0) == '-') {
			return false;
		}
		for (int i =0; i < length; i++) {
			char c = str.charAt(i);
			if (c <= '/' || c >= ':') {
				return false;
			}
		}
		return true;
	}
}
