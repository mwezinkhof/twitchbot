package bot.commands;

import bot.Bot;
import bot.Command;
import bot.handler.TemplateHandler;

public class Mod extends Command {

	public Mod(Bot b) {
		super(b);
		setCmd("mod");
		setOpOnly(true);
	}

	@Override
	public String execute(String[] args, String sender, boolean isOp) {
		if(isOp && args.length >= 1){
			bot.msg("Welcome to the mod club "+args[0]);
			return ".mod "+args[0];
		}
		return TemplateHandler.get("permission_denied");
	}
}
