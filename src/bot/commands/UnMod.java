package bot.commands;

import bot.Bot;
import bot.Command;
import bot.handler.TemplateHandler;

public class UnMod extends Command {

	public UnMod(Bot b) {
		super(b);
		setCmd("unmod");
		setOpOnly(true);
	}

	@Override
	public String execute(String[] args, String sender, boolean isOp) {
		if(isOp && args.length >= 1){
			bot.msg("You are no longer a mod "+args[0]+"! SwiftRage");
			return ".unmod "+args[0];
		}
		return TemplateHandler.get("permission_denied");
	}
}
