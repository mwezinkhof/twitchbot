package bot.commands;

import bot.Bot;
import bot.handler.TemplateHandler;

import java.util.Arrays;

public class CustomCommand extends bot.Command {
	private String[] valid_actions = {"add", "delete"};

	public CustomCommand(Bot b) {
		super(b);
		setOpOnly(true);
		setCmd("command");
		setDescription("The usage for !" + this.getCmd() + ": &lt;add/delete&gt; &lt;command&gt; &lt;action&gt;");
	}

	@Override
	public String execute(String[] args, String sender, boolean isOp) {
		if (args.length >= 2 && Arrays.asList(valid_actions).contains(args[0])) {
			if (isOp){
				if(args[0].equals("add") && args.length>2){
					String output="";
					for(int i=2;i<args.length;i++){
						output += args[i] + " ";
					}
					bot.addCustomCommand(args[1],output.trim());
					return "Added !"+args[1];
				}
				else if(args[0].equals("delete")){
					bot.removeCustomCommand(args[1]);
					return "Removed !"+args[1];
				}
			}
			else{
				return "Only ops.";
			}
		} else {
			return getDescription();
		}
		return TemplateHandler.get("permission_denied");
	}
}
