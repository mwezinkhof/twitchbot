package bot.commands;

import bot.Bot;
import bot.Command;
import bot.handler.TemplateHandler;

/**
 * Created by kaaz on 28-8-2014.
 */
public class Say  extends Command {
	public Say(Bot b) {
		super(b);
		setOpOnly(true);
		setCmd("say");
	}

	@Override
	public String execute(String[] args, String sender, boolean isOp) {
		boolean first=true;
		String ret = "";
		if(isOp && args.length >0){
			for(String s: args){
				if(first){
					first=false;
					ret += s;
				}
				else{
					ret += " " + s;
				}

			}
		}
		return TemplateHandler.get("permission_denied");
	}
}