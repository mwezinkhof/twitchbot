package bot.commands;

import bot.Bot;
import bot.Command;
import misc.IntUtil;

import java.util.Calendar;
import java.util.Date;

public class AwayFromKeyboard extends Command {

	private Calendar cal = Calendar.getInstance();

	public AwayFromKeyboard(Bot b) {
		super(b);
		setCmd("afk");
		setOpOnly(true);
		setDescription("!afk +minutes (+15 for instance)");
	}

	@Override
	public String execute(String[] args, String sender, boolean isOp) {
		if (isOp && args.length >= 1) {
			if (args[0].startsWith("+")) {
				if (IntUtil.isPositiveInteger(args[0].substring(1))) {
					int minutesAFK = Integer.parseInt(args[0].substring(1));
					cal.setTime(new Date());
					cal.add(Calendar.MINUTE, minutesAFK);
					return "AFK TILL " + cal.getTime().toString();
				} else {
					return "Use for instance: !" + getCmd() + " +15";
				}
			} else {
				return getDescription();
			}
		} else {
			return "HOST MAY OR MAY NOT BE AFK :D";
		}
	}
}
