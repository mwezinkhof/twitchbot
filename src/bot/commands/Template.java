package bot.commands;

import bot.Bot;
import bot.Command;
import bot.handler.TemplateHandler;

public class Template extends Command {

	public Template(Bot b) {
		super(b);
		setOpOnly(true);
		setCmd("template");
	}

	@Override
	public String execute(String[] args, String sender, boolean isOp) {
		if (isOp) {
			if (args.length >= 2) {
				String templateText = "";
				for (int i = 1; i < args.length; i++) {
					templateText += " "+args[i];
				}
				TemplateHandler.add(args[0], templateText.trim());
				return "Added template!";
			}
			return "Invalid arguments! user !template keyphrase text";
		}
		return TemplateHandler.get("permission_denied");
	}
}
