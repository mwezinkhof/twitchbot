package bot.commands;

import bot.Bot;
import bot.Command;
import bot.handler.TemplateHandler;

/**
 * Created by kaaz on 4-9-2014.
 */
public class TimedMessage extends Command{
	public TimedMessage(Bot b){
		super(b);
		setOpOnly(true);
		setCmd("timedmsg");
	}
	@Override
	public String execute(String[] args, String sender, boolean isOp) {
		if(isOp){

		}
		return TemplateHandler.get("permission_denied");
	}
}
