package bot.commands;

import bot.Bot;
import bot.Command;
import bot.handler.TemplateHandler;
import main.Config;
import org.jibble.pircbot.User;

public class GetUserList extends Command {

	public GetUserList(Bot b) {
		super(b);
		setCmd("userlist");
		setOpOnly(true);
	}

	@Override
	public String execute(String[] args, String sender, boolean isOp) {
		if(isOp){
			String userString="";
			for(User u:bot.getUsers("#"+Config.get("bot.channel"))){
				userString+= " "+u.getNick();
			}
			int size =bot.getUsers("#"+Config.get("bot.channel")).length;
			return "("+size+")"+userString;
		}
		return TemplateHandler.get("permission_denied");
	}
}
