package db.sqlite.table;

public enum ColumnType {
    INTEGER("integer"),
    TEXT("text"),
    REAL("real"),
    BLOB("blob");

    private ColumnType(final String colType) {
        this.colType = colType;
    }

    private final String colType;

    @Override
    public String toString() {
        return colType;
    }
}
