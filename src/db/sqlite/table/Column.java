package db.sqlite.table;

public class Column {

    String name = "id";
    String type = "integer";
    boolean isNullable = true;
    boolean isPrimaryKey = false;

    public boolean isPrimaryKey() {
        return isPrimaryKey;
    }

    public void setPrimaryKey(boolean isPrimaryKey) {
        this.isPrimaryKey = isPrimaryKey;
        setNullable(false);
    }


    public Column(String name, ColumnType type) {
        this.name = name;
        this.type = type.toString();
    }

    public Column(String name, ColumnType type, boolean canBeNull) {
        this.name = name;
        this.type = type.toString();
        this.isNullable = canBeNull;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(ColumnType type) {
        this.type = type.toString().toLowerCase();
    }

    public boolean isNullable() {
        return isNullable;
    }

    public void setNullable(boolean nullable) {
        this.isNullable = nullable;
    }


    @Override
    public String toString() {
        return name
                + " " + type
                + (isPrimaryKey ? " primary key" : "")
                + ((isPrimaryKey && type.equals("integer") ? " autoincrement" : ""))
                + (isNullable ? " not null" : "");
    }
}
