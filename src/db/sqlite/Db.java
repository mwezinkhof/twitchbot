package db.sqlite;

import org.reflections.Reflections;

import java.lang.reflect.InvocationTargetException;
import java.sql.*;
import java.util.Set;

public class Db {

    private static Connection c;

    private static Connection getConnection(){
        if(!(c instanceof Connection)){
            try {
                Class.forName("org.sqlite.JDBC");
                c = DriverManager.getConnection("jdbc:sqlite:kaazinator.db");
                c.setAutoCommit(true);
            } catch ( Exception e ) {
                System.err.println( e.getClass().getName() + ": " + e.getMessage() );
                System.exit(0);
            }
            System.out.println("Opened database successfully");
        }
        return c;
    }
    public static ResultSet select(String sql){
        Statement stmt;
        try {
            stmt = getConnection().createStatement();
            return stmt.executeQuery( sql );
        } catch ( Exception e ) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
        }
        return null;
    }
    public static void query(String sql){

        Statement stmt;
        try {
            stmt = getConnection().createStatement();
            stmt.executeUpdate(sql);
            stmt.close();
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public static void createDatabase(){
        Reflections reflections = new Reflections("db.sqlite.scheme");
        Set<Class<? extends SimpleSchema>> classes = reflections.getSubTypesOf(SimpleSchema.class);
        for (Class<? extends SimpleSchema> mc : classes) {
            if (mc.getSuperclass() == SimpleSchema.class) {
                try {
                    SimpleSchema sc = mc.getConstructor().newInstance();
                    query(sc.getSchema());
                    System.out.println(sc.getSchema());
                } catch (NoSuchMethodException | InvocationTargetException | InstantiationException | IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }

    }
}