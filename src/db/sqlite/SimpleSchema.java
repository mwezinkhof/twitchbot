package db.sqlite;

import db.sqlite.table.Column;
import db.sqlite.table.ColumnType;

import java.util.ArrayList;

/**
 * Created by Maik Wezinkhof on 27-3-14
 */
public abstract class SimpleSchema {

    protected String tableName = "example";
    protected Column primaryKey;
    protected ArrayList<Column> columns;

    protected void setTableName(String tblname) {
        this.tableName = tblname;
    }

    public String getTableName() {
        return tableName;
    }
    public SimpleSchema() {
        tableName = "example";
        primaryKey = new Column("id", ColumnType.INTEGER, true);
        primaryKey.setPrimaryKey(true);
        columns = new ArrayList<>();
    }

    public void removeColumn(String colName) {
        Column c = null;
        for (Column tc : columns) {
            if (tc.getName().equalsIgnoreCase(colName)) {
                c = tc;
                break;
            }
        }
        if (c != null) {
            columns.remove(c);
        }
    }
    public void addColumn(Column c) {
        columns.add(c);
    }

    public String getSchema() {
        String tbl = "";
        if (columns.size() > 0) {
            tbl += "CREATE TABLE IF NOT EXISTS " + tableName + "(";
            tbl += primaryKey.toString();
            for (Column c : columns) {
                tbl += ", " + c.toString();
            }
            tbl += ")";
        }
        return tbl;
    }
}
