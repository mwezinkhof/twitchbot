package db.sqlite.scheme;

import db.sqlite.SimpleSchema;
import db.sqlite.table.Column;
import db.sqlite.table.ColumnType;

public class CommandScheme extends SimpleSchema {

        public CommandScheme() {
            super();
            setTableName("command");
            addColumn(new Column("command", ColumnType.TEXT, false));
            addColumn(new Column("result", ColumnType.TEXT, false));
        }
}
