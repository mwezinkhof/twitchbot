package db.mysql;

import com.mysql.jdbc.jdbc2.optional.MysqlConnectionPoolDataSource;
import main.Config;
import org.apache.log4j.Logger;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;

/**
 * Created by kaaz on 28-8-2014.
 */
public class Db {
	private final static Logger log = Logger.getLogger(Db.class);
	private static Connection c;
	private DataSource dataSource;

	private static Connection createConnection() {
		try {
			MysqlConnectionPoolDataSource dataSource = new MysqlConnectionPoolDataSource();
			dataSource.setUser(Config.get("database.username"));
			dataSource.setPassword(Config.get("database.password"));
			dataSource.setServerName(Config.get("database.hostname"));
			dataSource.setPort(Integer.parseInt(Config.get("database.port")));
			dataSource.setDatabaseName(Config.get("database.database"));
			return dataSource.getConnection();
		} catch (SQLException e) {
			Logger.getLogger(Db.class).fatal("Can't connect to the database! Make sure the database settings are corrent and the database server is running");
		}
		return null;
	}

	static public boolean canConnect() {
		if (getConnection() != null) {
			try {
				return !getConnection().isClosed();
			} catch (SQLException e) {
			}
		}
		return false;
	}

	static public Connection getConnection() {
		if (c == null) {
			c = createConnection();
		}
		return c;
	}

	public static ResultSet select(String sql) {
		Statement stmt;
		try {
			stmt = getConnection().createStatement();
			return stmt.executeQuery(sql);
		} catch (SQLException e) {
			Logger.getLogger(Db.class).fatal(e.getMessage());
		}
		return null;
	}

	public static ResultSet select(String sql, Object... params) {
		PreparedStatement query = null;
		try {
			query = getConnection().prepareStatement(sql);
			int index = 1;
			for (Object p : params) {
				if (p instanceof String) {
					query.setString(index, (String) p);
				} else if (p instanceof Integer) {
					query.setInt(index, (int) p);
				} else {
					log.warn("Unknown type!" + p);
				}
				index++;
			}
			return query.executeQuery();

		} catch (SQLException e) {
			Logger.getLogger(Db.class).fatal(e.getMessage());
			e.printStackTrace();
		}
		return null;
	}

	public static void query(String sql) {

		Statement stmt;
		try {
			stmt = getConnection().createStatement();
			stmt.executeUpdate(sql);
			stmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void query(String sql, ArrayList<String> params) {
		PreparedStatement query = null;
		try {
			query = getConnection().prepareStatement(sql);
			int index = 1;
			for (String p : params) {
				if (p instanceof String) {
					query.setString(index, p);
				}
				index++;
			}
			query.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void query(String sql, Object... params) {
		PreparedStatement query = null;
		try {
			query = getConnection().prepareStatement(sql);
			int index = 1;
			for (Object p : params) {
				if (p instanceof String) {
					query.setString(index, (String) p);
				} else if (p instanceof Integer) {
					query.setInt(index, (int) p);
				} else {
					log.warn("Unknown type!" + p);
				}
				index++;
			}
			query.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
