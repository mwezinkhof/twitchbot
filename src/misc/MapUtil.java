package misc;

import java.util.Map;

/**
 * Utility functions for HashMaps and ArrayLists
 */

public class MapUtil {

	public static <K> String[] mapKeysToArray(Map<String, K> map) {
		String[] ret = new String[map.keySet().size()];
		int i = 0;
		for (String key : map.keySet()) {
			ret[i++] = key;
		}
		return ret;
	}
}
