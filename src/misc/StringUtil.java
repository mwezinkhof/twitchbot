package misc;

import java.util.ArrayList;

/**
 * Utility functions for text
 */
public class StringUtil {
	/**
	 * Converts an ArrayList to a readable formatted list
	 * For instance: A, B, C and D
	 * @param list
	 * @return formatted String
	 */
	public static String enumerateList(ArrayList<String> list){
		String text="";
		int size = list.size();
		int i = 0;
		boolean first=true;
		if(size == 1){
			return list.get(0);
		}
		for(String entry : list){
			i++;
			if(first){
				first=false;
				text += entry;
			}
			else if (i == size){
				text += " and "+entry;
			}
			else{
				text += ", "+entry;
			}
		}
		return text;
	}
	public static String repeatQuestionMark(int numQs) {
		String items = "";
		for (int i = 0; i < numQs; i++) {
			if (i != 0) items += ", ";
			items += "?";
		}
		return items;
	}
}
