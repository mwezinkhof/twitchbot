package main;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * Configuration class
 */
public class Config {
	private static final Config instance = new Config();
	private String filename = "properties.cfg";
	private HashMap<String, String> defaultProperties;
	private HashMap<String, String> loadedProperties;

	private Config() {
		defaultProperties = new HashMap<>();
		loadedProperties = new HashMap<>();
		if (!propertyFileExists()) {
			createPropertyFile();
		}
		loadDefaultProps();
		loadProperties();
		saveProperties();
	}

	public static String get(String propName) {
		if (instance.loadedProperties.containsKey(propName)) {
			return instance.loadedProperties.get(propName);
		}
		return "";
	}

	public static Config getInstance() {
		return instance;
	}

	private void createPropertyFile() {
		File f = new File(filename);
		try {
			f.createNewFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void loadDefaultProps() {
		addDefaultProperty("database.hostname", "localhost");
		addDefaultProperty("database.port", "3306");
		addDefaultProperty("database.username", "twitch_db");
		addDefaultProperty("database.password", "mydatabasepassword");
		addDefaultProperty("database.database", "twitch");

		addDefaultProperty("irc.host", "irc.twitch.tv");
		addDefaultProperty("irc.port", "6667");

		addDefaultProperty("bot.name", "mybotname");
		addDefaultProperty("bot.channel", "mychannel");
		addDefaultProperty("bot.password", "mypassword");
	}

	private void addDefaultProperty(String key, String value) {
		if (!defaultProperties.containsKey(key)) {
			defaultProperties.put(key, value);
		}
	}

	private boolean propertyFileExists() {
		File f = new File(filename);
		return f.exists() && !f.isDirectory();
	}

	private void loadProperties() {
		Properties prop = new SortedProperty();
		InputStream input = null;
		loadedProperties = new HashMap<>();

		try {
			input = new FileInputStream(filename);
			prop.load(input);
			for (Map.Entry<String, String> def : defaultProperties.entrySet()) {
				loadedProperties.put(def.getKey(), prop.getProperty(def.getKey(), def.getValue()));
			}

		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

	}

	public void saveProperties() {
		Properties prop = new SortedProperty();
		OutputStream output = null;

		try {

			output = new FileOutputStream(filename);
			for (Map.Entry<String, String> def : loadedProperties.entrySet()) {
				prop.setProperty(def.getKey(), def.getValue());
			}
			prop.store(output, null);

		} catch (IOException io) {
			io.printStackTrace();
		} finally {
			if (output != null) {
				try {
					output.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

		}
	}
}
